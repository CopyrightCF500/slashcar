#! /bin/sh

colcon build --continue-on-error --cmake-target clean 
rm -rf build* install* log* lcov*
py3clean .
