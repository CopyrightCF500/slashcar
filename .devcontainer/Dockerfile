FROM ros:humble-ros-base-jammy

ENV ROS_DISTRO=humble
ENV ROS_VERSION=2
ENV ROS_PYTHON_VERSION=3

# Install ROS2 packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-${ROS_DISTRO}-perception=0.10.0-1* \
    && rm -rf /var/lib/apt/lists/*

# Install Tool and other packages
RUN apt-get update \
    && apt-get install -y bash-completion \
        build-essential \
        cmake \
        gdb \
        git \
        wget \
        htop \
        vim \
        python3-pip \
        sudo \
        fakeroot \
    && apt-get autoremove -y \
    && apt-get clean -y

ARG USERNAME=dev
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create a non-root user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && usermod -a -G dialout $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && echo "source /usr/share/bash-completion/completions/git" >> /home/$USERNAME/.bashrc \
    && echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> /home/$USERNAME/.bashrc 

ARG WORKSPACE=/workspaces/SlashCar

RUN echo "if [ -f ${WORKSPACE}/install/setup.bash ]; then source ${WORKSPACE}/install/setup.bash; fi" >> /home/$USERNAME/.bashrc

USER $USERNAME